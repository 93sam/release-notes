# German translation of the Debian release notes
#
# Jan Hauke Rahm <info@jhr-online.de>, 2009.
# Holger Wansing <linux@wansing-online.de>, 2010, 2011, 2013, 2015, 2017.
# Holger Wansing <hwansing@mailbox.org>, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 9.0\n"
"POT-Creation-Date: 2021-04-26 22:45+0200\n"
"PO-Revision-Date: 2021-04-14 23:01+0100\n"
"Last-Translator: Holger Wansing <hwansing@mailbox.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#. type: Attribute 'lang' of: <chapter>
#: en/about.dbk:8
msgid "en"
msgstr "de"

#. type: Content of: <chapter><title>
#: en/about.dbk:9
msgid "Introduction"
msgstr "Einführung"

#. type: Content of: <chapter><para>
#: en/about.dbk:11
msgid ""
"This document informs users of the &debian; distribution about major changes "
"in version &release; (codenamed &releasename;)."
msgstr ""
"Dieses Dokument informiert Benutzer der &debian;-Distribution über "
"entscheidende Änderungen in Version &release; (Codename &Releasename;)."

#. type: Content of: <chapter><para>
#: en/about.dbk:15
msgid ""
"The release notes provide information on how to upgrade safely from release "
"&oldrelease; (codenamed &oldreleasename;) to the current release and inform "
"users of known potential issues they could encounter in that process."
msgstr ""
"Die Hinweise zur Veröffentlichung enthalten Informationen, wie ein sicheres "
"Upgrade von Version &oldrelease; (Codename &Oldreleasename;) auf die "
"aktuelle Veröffentlichung durchgeführt werden kann und informieren die "
"Benutzer über bekannte potenzielle Probleme, die während des Upgrades "
"auftreten können."

#. type: Content of: <chapter><para>
#: en/about.dbk:21
msgid ""
"You can get the most recent version of this document from <ulink url=\"&url-"
"release-notes;\"></ulink>."
msgstr ""
"Die neueste Version dieses Dokuments erhalten Sie unter <ulink url=\"&url-"
"release-notes;\"></ulink>."

#. type: Content of: <chapter><caution><para>
#: en/about.dbk:26
msgid ""
"Note that it is impossible to list every known issue and that therefore a "
"selection has been made based on a combination of the expected prevalence "
"and impact of issues."
msgstr ""
"Beachten Sie, dass es unmöglich ist, alle bekannten Probleme aufzulisten; "
"deshalb wurde eine Auswahl getroffen, basierend auf einer Kombination aus "
"der zu erwartenden Häufigkeit des Auftretens und der Auswirkung der Probleme."

#. type: Content of: <chapter><para>
#: en/about.dbk:32
msgid ""
"Please note that we only support and document upgrading from the previous "
"release of Debian (in this case, the upgrade from &oldreleasename;).  If you "
"need to upgrade from older releases, we suggest you read previous editions "
"of the release notes and upgrade to &oldreleasename; first."
msgstr ""
"Bitte gestatten Sie uns die Anmerkung, dass wir lediglich ein Upgrade von "
"der letzten Version (in diesem Fall &Oldreleasename;) auf die aktuelle "
"unterstützen können. Falls Sie ein Upgrade von einer noch älteren Version "
"durchführen müssen, empfehlen wir dringend, dass Sie die früheren Ausgaben "
"der Veröffentlichungshinweise lesen und zuerst ein Upgrade auf "
"&Oldreleasename; durchführen."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:40
msgid "Reporting bugs on this document"
msgstr "Fehler in diesem Dokument berichten"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:42
msgid ""
"We have attempted to test all the different upgrade steps described in this "
"document and to anticipate all the possible issues our users might encounter."
msgstr ""
"Wir haben versucht, die einzelnen Schritte des Upgrades in diesem Dokument "
"zu beschreiben und alle möglicherweise auftretenden Probleme vorherzusehen."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:47
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or "
"information that is missing)  in this documentation, please file a bug in "
"the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
"<systemitem role=\"package\">release-notes</systemitem> package. You might "
"first want to review the <ulink url=\"&url-bts-rn;\">existing bug reports</"
"ulink> in case the issue you've found has already been reported. Feel free "
"to add additional information to existing bug reports if you can contribute "
"content for this document."
msgstr ""
"Falls Sie dennoch einen Fehler in diesem Dokument gefunden haben "
"(fehlerhafte oder fehlende Informationen), senden Sie bitte einen "
"entsprechenden Fehlerbericht über das Paket <systemitem role=\"package"
"\">release-notes</systemitem> an unsere <ulink url=\"&url-bts;"
"\">Fehlerdatenbank</ulink>. Sie können auch zunächst die <ulink url=\"&url-"
"bts-rn;\">bereits vorhandenen Fehlerberichte</ulink> lesen für den Fall, "
"dass das Problem, welches Sie gefunden haben, schon berichtet wurde. Sie "
"dürfen gerne zusätzliche Informationen zu solchen bereits vorhandenen "
"Fehlerberichten hinzufügen, wenn Sie Inhalte zu diesem Dokument beitragen "
"können."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:59
msgid ""
"We appreciate, and encourage, reports providing patches to the document's "
"sources. You will find more information describing how to obtain the sources "
"of this document in <xref linkend=\"sources\"/>."
msgstr ""
"Wir begrüßen Fehlerberichte, die Patches für den Quellcode des Dokuments "
"bereitstellen und möchten Sie sogar dazu ermuntern, solche einzureichen. "
"Mehr Informationen darüber, wie Sie den Quellcode bekommen, finden Sie in "
"<xref linkend=\"sources\"/>."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:67
msgid "Contributing upgrade reports"
msgstr "Upgrade-Berichte zur Verfügung stellen"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:69
msgid ""
"We welcome any information from users related to upgrades from "
"&oldreleasename; to &releasename;.  If you are willing to share information "
"please file a bug in the <ulink url=\"&url-bts;\">bug tracking system</"
"ulink> against the <systemitem role=\"package\">upgrade-reports</systemitem> "
"package with your results.  We request that you compress any attachments "
"that are included (using <command>gzip</command>)."
msgstr ""
"Wir begrüßen jede Information von unseren Benutzern, die sich auf ein "
"Upgrade von &Oldreleasename; auf &Releasename; bezieht. Falls Sie solche "
"Informationen bereitstellen möchten, senden Sie bitte einen Fehlerbericht "
"mit den entsprechenden Informationen gegen das Paket <systemitem role="
"\"package\">upgrade-reports</systemitem> an unsere <ulink url=\"&url-bts;"
"\">Fehlerdatenbank</ulink>. Wir bitten Sie, alle Anhänge, die Sie Ihrem "
"Bericht beifügen, zu komprimieren (mit dem Befehl <command>gzip</command>)."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:78
msgid ""
"Please include the following information when submitting your upgrade report:"
msgstr "Bitte fügen Sie Ihrem Upgrade-Bericht folgende Informationen bei:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:85
msgid ""
"The status of your package database before and after the upgrade: "
"<systemitem role=\"package\">dpkg</systemitem>'s status database available "
"at <filename>/var/lib/dpkg/status</filename> and <systemitem role=\"package"
"\">apt</systemitem>'s package state information, available at <filename>/var/"
"lib/apt/extended_states</filename>.  You should have made a backup before "
"the upgrade as described at <xref linkend=\"data-backup\"/>, but you can "
"also find backups of <filename>/var/lib/dpkg/status</filename> in <filename>/"
"var/backups</filename>."
msgstr ""
"Den Status Ihrer Paketdatenbank vor und nach dem Upgrade: Die "
"Statusdatenbank von <systemitem role=\"package\">dpkg</systemitem> finden "
"Sie unter <filename>/var/lib/dpkg/status</filename>, die "
"Paketstatusinformationen von <systemitem role=\"package\">apt</systemitem> "
"unter <filename>/var/lib/apt/extended_states</filename>. Sie sollten vor dem "
"Upgrade eine Sicherung dieser Daten erstellen (wie unter <xref linkend="
"\"data-backup\"/> beschrieben). Sicherungen von <filename>/var/lib/dpkg/"
"status</filename> sind aber auch in <filename>/var/backups</filename> zu "
"finden."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:98
msgid ""
"Session logs created using <command>script</command>, as described in <xref "
"linkend=\"record-session\"/>."
msgstr ""
"Upgrade-Protokolle, erstellt mit Hilfe des Befehls <command>script</command> "
"(wie in <xref linkend=\"record-session\"/> beschrieben)."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:104
msgid ""
"Your <systemitem role=\"package\">apt</systemitem> logs, available at "
"<filename>/var/log/apt/term.log</filename>, or your <command>aptitude</"
"command> logs, available at <filename>/var/log/aptitude</filename>."
msgstr ""
"Ihre <systemitem role=\"package\">apt</systemitem>-Logdateien, die Sie unter "
"<filename>/var/log/apt/term.log</filename> finden, oder Ihre "
"<command>aptitude</command>-Logdateien, die unter <filename>/var/log/"
"aptitude</filename> zu finden sind."

#. type: Content of: <chapter><section><note><para>
#: en/about.dbk:113
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug report "
"as the information will be published in a public database."
msgstr ""
"Sie sollten sich ein wenig Zeit nehmen, um die Informationen zu prüfen und "
"sensible bzw. vertrauliche Daten aus den Logdateien zu löschen, bevor Sie "
"die Informationen dem Fehlerbericht anhängen, da der gesamte Bericht mit "
"Ihren Anhängen öffentlich gespeichert und einsehbar sein wird."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:122
msgid "Sources for this document"
msgstr "Quelltext dieses Dokuments"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:124
msgid ""
"The source of this document is in DocBook XML<indexterm><primary>DocBook "
"XML</primary></indexterm> format. The HTML version is generated using "
"<systemitem role=\"package\">docbook-xsl</systemitem> and <systemitem role="
"\"package\">xsltproc</systemitem>. The PDF version is generated using "
"<systemitem role=\"package\">dblatex</systemitem> or <systemitem role="
"\"package\">xmlroff</systemitem>. Sources for the Release Notes are "
"available in the Git repository of the <emphasis>Debian Documentation "
"Project</emphasis>.  You can use the <ulink url=\"&url-vcs-release-notes;"
"\">web interface</ulink> to access its files individually through the web "
"and see their changes.  For more information on how to access Git please "
"consult the <ulink url=\"&url-ddp-vcs-info;\">Debian Documentation Project "
"VCS information pages</ulink>."
msgstr ""
"Die Quellen für dieses Dokument liegen im DocBook-"
"XML<indexterm><primary>DocBook XML</primary></indexterm>-Format vor. Die "
"HTML-Version wird mit <systemitem role=\"package\">docbook-xsl</systemitem> "
"und <systemitem role=\"package\">xsltproc</systemitem> erstellt. Die PDF-"
"Version wird mit <systemitem role=\"package\">dblatex</systemitem> oder "
"<systemitem role=\"package\">xmlroff</systemitem> erstellt. Die Quellen der "
"Veröffentlichungshinweise sind im GIT-Depot des <emphasis>Debian-"
"Dokumentationsprojekts</emphasis> verfügbar. Sie können die <ulink url="
"\"&url-vcs-release-notes;\">Web-Oberfläche</ulink> nutzen, um die einzelnen "
"Dateien und ihre Änderungen einzusehen. Für weitere Informationen zum Umgang "
"mit GIT beachten Sie bitte die <ulink url=\"&url-ddp-vcs-info;\">GIT-"
"Informationsseiten</ulink> des Debian-Dokumentationsprojekts."
