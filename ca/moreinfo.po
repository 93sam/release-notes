# Catalan translation of the Debian Release Notes
#
# Miguel Gea Milvaques, 2006-2009.
# Jordà Polo, 2007, 2009.
# Guillem Jover <guillem@debian.org>, 2007, 2019.
# Héctor Orón Martínez, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 10\n"
"POT-Creation-Date: 2019-06-15 19:17+0200\n"
"PO-Revision-Date: 2020-12-09 15:24+0100\n"
"Last-Translator: Alex Muntada <alexm@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#. type: Attribute 'lang' of: <chapter>
#: en/moreinfo.dbk:8
msgid "en"
msgstr "ca"

#. type: Content of: <chapter><title>
#: en/moreinfo.dbk:9
msgid "More information on &debian;"
msgstr "Més informació sobre &debian;"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:11
msgid "Further reading"
msgstr "Llegir més"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:13
msgid ""
"Beyond these release notes and the installation guide, further documentation "
"on Debian is available from the Debian Documentation Project (DDP), whose "
"goal is to create high-quality documentation for Debian users and "
"developers, such as the Debian Reference, Debian New Maintainers Guide, the "
"Debian FAQ, and many more.  For full details of the existing resources see "
"the <ulink url=\"&url-ddp;\">Debian Documentation website</ulink> and the "
"<ulink url=\"&url-wiki;\">Debian Wiki</ulink>."
msgstr ""
"Més enllà d'estes notes de llançament i de la guia d'instal·lació, hi ha més "
"documentació de &debian; a la vostra disposició al Projecte de Documentació "
"de Debian (DDP), que té com a objectiu crear documentació de gran qualitat "
"pels usuaris i desenvolupadors de Debian. Esta documentació inclou la guia "
"de referència de Debian, la guia del nou mantenidor, les PMF de Debian i "
"molt més. Per obtenir informació detallada dels recursos al vostre abast, "
"visiteu el <ulink url=\"&url-ddp;\">lloc web del projecte de documentació de "
"Debian</ulink> i el lloc <ulink url=\"&url-wiki;\">Viqui de Debian</ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:23
msgid ""
"Documentation for individual packages is installed into <filename>/usr/share/"
"doc/<replaceable>package</replaceable></filename>.  This may include "
"copyright information, Debian specific details, and any upstream "
"documentation."
msgstr ""
"Trobareu informació de cada paquet instal·lada a <filename>/usr/share/doc/"
"<replaceable>paquet</replaceable></filename>. Esta pot incloure informació "
"dels drets d'autor, detalls específics de Debian i la documentació del "
"desenvolupament principal."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:31
msgid "Getting help"
msgstr "Trobar ajuda"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:33
msgid ""
"There are many sources of help, advice, and support for Debian users, though "
"these should only be considered after researching the issue in available "
"documentation.  This section provides a short introduction to these sources "
"which may be helpful for new Debian users."
msgstr ""
"Hi ha moltes fonts d'ajuda, consells i suport pels usuaris de Debian, però "
"tan sols hauria de considerar-se si no l'heu aconseguit a cap lloc després "
"de la recerca dins la documentació sobre problema. Esta secció proporciona "
"una introducció curta a les que poden ser d'ajuda pels usuaris de Debian."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:39
msgid "Mailing lists"
msgstr "Llistes de correu"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:41
msgid ""
"The mailing lists of most interest to Debian users are the debian-user list "
"(English) and other debian-user-<replaceable>language</replaceable> lists "
"(for other languages).  For information on these lists and details of how to "
"subscribe see <ulink url=\"&url-debian-list-archives;\"></ulink>.  Please "
"check the archives for answers to your question prior to posting and also "
"adhere to standard list etiquette."
msgstr ""
"Les llistes de correu més interessants pels usuaris de Debian són la llista "
"debian-user (en anglès) i altres llistes com debian-user-"
"<replaceable>catalan</replaceable> (en català). Per obtenir més informació "
"d'estes llistes i detalls de com subscriure's, vegeu <ulink url=\"&url-"
"debian-list-archives;\"></ulink>. Abans d'enviar la vostra pregunta, "
"comproveu els arxius amb les respostes i respecteu les normes de la llista."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:51
msgid "Internet Relay Chat"
msgstr "Internet Relay Chat"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:53
msgid ""
"Debian has an IRC channel dedicated to support and aid for Debian users, "
"located on the OFTC IRC network.  To access the channel, point your favorite "
"IRC client at irc.debian.org and join <literal>#debian</literal>."
msgstr ""
"Debian té un canal IRC dedicat al suport i ajuda als usuaris de Debian que "
"es troba a la xarxa d'IRC OFTC. Per accedir al canal, dirigiu el vostre "
"client IRC a irc.debian.org i uniu-vos a <literal>#debian</literal>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:58
msgid ""
"Please follow the channel guidelines, respecting other users fully.  The "
"guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."
msgstr ""
"Seguiu les normes del canal, respectant completament els altres usuaris. Les "
"normes les podeu trobar al <ulink url=\"&url-wiki;DebianIRC\">Viqui de "
"Debian</ulink>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:63
msgid ""
"For more information on OFTC please visit the <ulink url=\"&url-irc-host;"
"\">website</ulink>."
msgstr ""
"Per obtenir més informació d'OFTC visiteu la pàgina web del <ulink url="
"\"&url-irc-host;\">lloc web</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:71
msgid "Reporting bugs"
msgstr "Informes d'error"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:73
msgid ""
"We strive to make Debian a high-quality operating system; however that does "
"not mean that the packages we provide are totally free of bugs.  Consistent "
"with Debian's <quote>open development</quote> philosophy and as a service to "
"our users, we provide all the information on reported bugs at our own Bug "
"Tracking System (BTS).  The BTS can be browsed at <ulink url=\"&url-bts;\"></"
"ulink>."
msgstr ""
"Ens esforcem per fer de &debian; un sistema operatiu de gran qualitat, però "
"això no vol dir que els paquets que subministrem estiguen totalment lliures "
"d'errors. D'acord amb la filosofia de <quote>desenvolupament obert</quote> "
"de Debian, i com un servei als nostres usuaris, facilitem tota la informació "
"dels errors que s'han rebut en el nostre sistema de seguiment d'errors "
"(BTS). Podeu navegar pel BTS a <ulink url=\"&url-bts;\"></ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:81
msgid ""
"If you find a bug in the distribution or in packaged software that is part "
"of it, please report it so that it can be properly fixed for future "
"releases.  Reporting bugs requires a valid e-mail address.  We ask for this "
"so that we can trace bugs and developers can get in contact with submitters "
"should additional information be needed."
msgstr ""
"Si trobeu alguna errada a la distribució o a algun paquet de programari que "
"forme part d'ella, informeu-ne de manera que es puga resoldre de la millor "
"manera en algun llançament posterior. L'enviament d'informes d'errors "
"requereix d'una adreça vàlida de correu electrònic. El demanem perquè així "
"podrem fer seguiment d'errors i els desenvolupadors podran posar-se en "
"contacte amb els que l'han enviat per si és necessària alguna informació "
"addicional."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:88
msgid ""
"You can submit a bug report using the program <command>reportbug</command> "
"or manually using e-mail.  You can find out more about the Bug Tracking "
"System and how to use it by reading the reference documentation (available "
"at <filename>/usr/share/doc/debian</filename> if you have <systemitem role="
"\"package\">doc-debian</systemitem> installed) or online at the <ulink url="
"\"&url-bts;\">Bug Tracking System</ulink>."
msgstr ""
"Podeu enviar un informe d'error utilitzant el programa <command>reportbug</"
"command> o be utilitzant manualment el correu electrònic. Podeu llegir més "
"informació del sistema de seguiment d'errors i de com utilitzar-lo llegint "
"la documentació de referència (que trobareu a <filename>/usr/share/doc/"
"debian</filename> si teniu el paquet <systemitem role=\"package\">doc-"
"debian</systemitem> instal·lat) o en línia al <ulink url=\"&url-bts;"
"\">sistema de seguiment d'errors</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:98
msgid "Contributing to Debian"
msgstr "Col·laborar amb Debian"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:100
msgid ""
"You do not need to be an expert to contribute to Debian.  By assisting users "
"with problems on the various user support <ulink url=\"&url-debian-list-"
"archives;\">lists</ulink> you are contributing to the community.  "
"Identifying (and also solving) problems related to the development of the "
"distribution by participating on the development <ulink url=\"&url-debian-"
"list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
"Debian's high-quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
"ulink> and help developers track them down and fix them.  The tool "
"<systemitem role=\"package\">how-can-i-help</systemitem> helps you to find "
"suitable reported bugs to work on.  If you have a way with words then you "
"may want to contribute more actively by helping to write <ulink url=\"&url-"
"ddp-vcs-info;\">documentation</ulink> or <ulink url=\"&url-debian-i18n;"
"\">translate</ulink> existing documentation into your own language."
msgstr ""
"No necessiteu ser experts per col·laborar amb Debian. Ajudant els usuaris en "
"problemes a les diferents <ulink url=\"&url-debian-list-archives;\">llistes</"
"ulink> de suport pels usuaris, ja esteu contribuint a la comunitat. "
"Identificant (i també resolent) els problemes associats al desenvolupament "
"de la distribució mitjançant la participació a les <ulink url=\"&url-debian-"
"list-archives;\">llistes</ulink> de desenvolupament també és de molta ajuda. "
"Per mantenir l'alta qualitat de la distribució Debian, <ulink url=\"&url-bts;"
"\">envieu els informes d'errors</ulink> i ajudeu els desenvolupadors a fer "
"el seu seguiment per corregir-los. Si teniu traça amb les paraules aleshores "
"potser voldreu col·laborant de forma més activa ajudant a escriure <ulink "
"url=\"&url-ddp-vcs-info;\">documentació</ulink> o <ulink url=\"&url-debian-"
"i18n;\">traduint</ulink> la que ja existeix en el vostre idioma."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:117
msgid ""
"If you can dedicate more time, you could manage a piece of the Free Software "
"collection within Debian.  Especially helpful is if people adopt or maintain "
"items that people have requested for inclusion within Debian.  The <ulink "
"url=\"&url-wnpp;\">Work Needing and Prospective Packages database</ulink> "
"details this information.  If you have an interest in specific groups then "
"you may find enjoyment in contributing to some of Debian's <ulink url=\"&url-"
"debian-projects;\">subprojects</ulink> which include ports to particular "
"architectures and <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> for specific user groups, among many others."
msgstr ""
"Si podeu dedicar més temps, podríeu gestionar una de les peces de programari "
"lliure de la col·lecció a Debian. És de molta ajuda que la gent adopte o "
"mantinga elements que la gent ha demanat per que s'incloga a Debian. La "
"<ulink url=\"&url-wnpp;\">base de dades de paquets en perspectiva i amb "
"feina pendent</ulink> detalla tota esta informació. Si teniu interès en "
"grups especials aleshores trobareu més diversió col·laborant amb alguns "
"<ulink url=\"&url-debian-projects;\">subprojectes</ulink> de Debian que "
"inclouen ports a arquitectures concretes, <ulink url=\"&url-debian-blends;"
"\">barreges pures de Debian</ulink> per a grups d'usuaris específics, entre "
"molts altres."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:128
msgid ""
"In any case, if you are working in the free software community in any way, "
"as a user, programmer, writer, or translator you are already helping the "
"free software effort.  Contributing is rewarding and fun, and as well as "
"allowing you to meet new people it gives you that warm fuzzy feeling inside."
msgstr ""
"De qualsevol manera, si esteu treballant a la comunitat de programari lliure "
"d'esta manera, com a usuari, programador, escrivint o traduint, ja esteu "
"ajudant a l'esforç pel programari lliure. Col·laborar és gratificant i "
"divertit, i també us permet conèixer nova gent que us donarà un càlid "
"sentiment de comunitat."
