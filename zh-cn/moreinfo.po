#
# Simplified Chinese translation of Lenny release notes
# Copyright (C) 2008 Debian Chinese project
# This file is distributed under the same license as the lenny release
# notes.
#
# Authors:
# chenxianren <chenxianren@gmail.com>, 2008.
# Dongsheng Song <dongsheng.song@gmail.com>, 2008-2009.
# LI Daobing <lidaobing@gmail.com>, 2008.
# Yangfl <mmyangfl@gmail.com>, 2017.
# Boyuan Yang <byang@debian.org>, 2019.
# Wenbin Lv <wenbin816@gmail.com>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 10.0\n"
"POT-Creation-Date: 2022-12-20 16:24+0800\n"
"PO-Revision-Date: 2022-12-20 21:47+0800\n"
"Last-Translator: Wenbin Lv <wenbin816@gmail.com>\n"
"Language-Team: Chinese (Simplified) <debian-l10n-chinese@lists.debian.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: Attribute 'lang' of: <chapter>
#: en/moreinfo.dbk:8
msgid "en"
msgstr "zh_CN"

#. type: Content of: <chapter><title>
#: en/moreinfo.dbk:9
msgid "More information on &debian;"
msgstr "关于 &debian; 的更多信息"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:11
msgid "Further reading"
msgstr "扩展阅读"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:13
msgid ""
"Beyond these release notes and the <ulink url=\"&url-install-manual;"
"\">installation guide</ulink>, further documentation on Debian is available "
"from the Debian Documentation Project (DDP), whose goal is to create high-"
"quality documentation for Debian users and developers, such as the Debian "
"Reference, Debian New Maintainers Guide, the Debian FAQ, and many more.  For "
"full details of the existing resources see the <ulink url=\"&url-ddp;"
"\">Debian Documentation website</ulink> and the <ulink url=\"&url-wiki;"
"\">Debian Wiki</ulink>."
msgstr ""
"除了本发行说明和<ulink url=\"&url-install-manual;\">安装指南</ulink>外，"
"Debian 文档项目（DDP）有更多关于 Debian 的文档，该项目的目标是为 Debian 用户"
"和开发者创建高质量的文档。它包括 Debian 参考手册、Debian 新维护人员指南、"
"Debian 常见问题及更多文档。如果您想了解这些资源的完整信息，请查阅 <ulink "
"url=\"&url-ddp;\">DDP 网站</ulink>和 <ulink url=\"&url-wiki;\">Debian 维基</"
"ulink>。"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:23
msgid ""
"Documentation for individual packages is installed into <filename>/usr/share/"
"doc/<replaceable>package</replaceable></filename>.  This may include "
"copyright information, Debian specific details, and any upstream "
"documentation."
msgstr ""
"各个软件包的文档被安装到 <filename>/usr/share/doc/<replaceable>软件包</"
"replaceable></filename>。它包括版权信息，特定于 Debian 的信息，以及上游的文"
"档。"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:31
msgid "Getting help"
msgstr "获得帮助"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:33
msgid ""
"There are many sources of help, advice, and support for Debian users, though "
"these should only be considered after researching the issue in available "
"documentation.  This section provides a short introduction to these sources "
"which may be helpful for new Debian users."
msgstr ""
"Debian 的用户可以从很多渠道获得帮助、建议和支持，但您只应该在研究并查阅文档了"
"解问题大致情况后再考虑寻求帮助。本节简单介绍了对 Debian 新用户可能会有帮助的"
"渠道。"

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:39
msgid "Mailing lists"
msgstr "邮件列表"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:41
msgid ""
"The mailing lists of most interest to Debian users are the debian-user list "
"(English) and other debian-user-<replaceable>language</replaceable> lists "
"(for other languages).  For information on these lists and details of how to "
"subscribe see <ulink url=\"&url-debian-list-archives;\"></ulink>.  Please "
"check the archives for answers to your question prior to posting and also "
"adhere to standard list etiquette."
msgstr ""
"邮件列表 debian-user（英语）和其它 debian-user-<replaceable>语言</"
"replaceable>（其它语言）中有 Debian 用户最感兴趣的内容。想了解这些邮件列表的"
"详细信息，以及如何订阅，请参阅 <ulink url=\"&url-debian-list-archives;\"></"
"ulink>。发问前请先在邮件列表存档中搜索答案，并注意遵守列表有关的礼仪和准则。"

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:51
msgid "Internet Relay Chat"
msgstr "IRC"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:53
msgid ""
"Debian has an IRC channel dedicated to support and aid for Debian users, "
"located on the OFTC IRC network.  To access the channel, point your favorite "
"IRC client at irc.debian.org and join <literal>#debian</literal>."
msgstr ""
"Debian 在 OFTC IRC 网络上有专门对 Debian 用户提供帮助的 IRC 频道。使用您喜欢"
"的 IRC 客户端连接 irc.debian.org，加入 <literal>#debian</literal> 频道即可。"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:58
msgid ""
"Please follow the channel guidelines, respecting other users fully.  The "
"guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."
msgstr ""
"请遵守频道的准则，尊敬其他用户。可以在 <ulink url=\"&url-wiki;"
"DebianIRC\">Debian 维基</ulink>中找到该准则。"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:63
msgid ""
"For more information on OFTC please visit the <ulink url=\"&url-irc-host;"
"\">website</ulink>."
msgstr ""
"欲了解更多关于 OFTC 的信息，请访问他们的<ulink url=\"&url-irc-host;\">网站</"
"ulink>。"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:71
msgid "Reporting bugs"
msgstr "报告 Bug"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:73
msgid ""
"We strive to make Debian a high-quality operating system; however that does "
"not mean that the packages we provide are totally free of bugs.  Consistent "
"with Debian's <quote>open development</quote> philosophy and as a service to "
"our users, we provide all the information on reported bugs at our own Bug "
"Tracking System (BTS).  The BTS can be browsed at <ulink url=\"&url-bts;\"></"
"ulink>."
msgstr ""
"我们努力让 Debian 成为一款高质量的操作系统，但这并不意味着我们提供的软件包完"
"全没有任何错误。为了和 Debian 一贯的<quote>开放的开发</quote>原则相呼应，也为"
"了能为我们的用户提供更好的服务，我们在错误追踪系统（BTS）提供全部已经报告的错"
"误的所有信息。您可以通过 <ulink url=\"&url-bts;\"></ulink> 来访问 BTS。"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:81
msgid ""
"If you find a bug in the distribution or in packaged software that is part "
"of it, please report it so that it can be properly fixed for future "
"releases.  Reporting bugs requires a valid e-mail address.  We ask for this "
"so that we can trace bugs and developers can get in contact with submitters "
"should additional information be needed."
msgstr ""
"如果您在本发行版或者软件包中发现了错误，请报告它，从而可以在将来的发行中被修"
"复。您需要一个有效的电子邮件地址才能报告错误。有这个限制是因为这样我们才能够"
"追踪错误，以及在开发人员需要更多信息时，能够与提交人联系。"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:88
msgid ""
"You can submit a bug report using the program <command>reportbug</command> "
"or manually using e-mail.  You can find out more about the Bug Tracking "
"System and how to use it by reading the reference documentation (available "
"at <filename>/usr/share/doc/debian</filename> if you have <systemitem "
"role=\"package\">doc-debian</systemitem> installed) or online at the <ulink "
"url=\"&url-bts;\">Bug Tracking System</ulink>."
msgstr ""
"您可以使用程序 <command>reportbug</command> 来提交一个错误报告，或者亲自动手"
"发送电子邮件。可以通过阅读参考文档（如果您安装了 <systemitem "
"role=\"package\">doc-debian</systemitem> 包的话，在 <filename>/usr/share/doc/"
"debian</filename> 可以找到）或者在线的<ulink url=\"&url-bts;\">错误追踪系统</"
"ulink>，以获得更多关于错误追踪系统的信息和用法。"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:98
msgid "Contributing to Debian"
msgstr "为 Debian 做贡献"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:100
msgid ""
"You do not need to be an expert to contribute to Debian.  By assisting users "
"with problems on the various user support <ulink url=\"&url-debian-list-"
"archives;\">lists</ulink> you are contributing to the community.  "
"Identifying (and also solving) problems related to the development of the "
"distribution by participating on the development <ulink url=\"&url-debian-"
"list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
"Debian's high-quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
"ulink> and help developers track them down and fix them.  The tool "
"<systemitem role=\"package\">how-can-i-help</systemitem> helps you to find "
"suitable reported bugs to work on.  If you have a way with words then you "
"may want to contribute more actively by helping to write <ulink url=\"&url-"
"ddp-vcs-info;\">documentation</ulink> or <ulink url=\"&url-debian-i18n;"
"\">translate</ulink> existing documentation into your own language."
msgstr ""
"并不是只有高手才能对 Debian 做出贡献。当您在各种支持用户的<ulink url=\"&url-"
"debian-list-archives;\">邮件列表</ulink>中帮助其他用户解决问题时，您就是在对"
"整个社区做贡献。参与开发<ulink url=\"&url-debian-list-archives;\">邮件列表</"
"ulink>以帮助开发者定位（及解决）和发行版开发相关的问题，对我们也是极大的帮"
"助。为了维持 Debian 系统一贯的高品质，请<ulink url=\"&url-bts;\">提交错误报告"
"</ulink>，并协助开发人员跟踪和修复它们。<systemitem role=\"package\">how-can-"
"i-help</systemitem> 工具可帮助您找到合适的已报告的错误供您解决。如果您有文字"
"方面的天赋，您也可以通过撰写<ulink url=\"&url-ddp-vcs-info;\">文档</ulink>，"
"或者<ulink url=\"&url-debian-i18n;\">翻译</ulink>现有文档到您自己的语言的方式"
"来做出贡献。"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:117
msgid ""
"If you can dedicate more time, you could manage a piece of the Free Software "
"collection within Debian.  Especially helpful is if people adopt or maintain "
"items that people have requested for inclusion within Debian.  The <ulink "
"url=\"&url-wnpp;\">Work Needing and Prospective Packages database</ulink> "
"details this information.  If you have an interest in specific groups then "
"you may find enjoyment in contributing to some of Debian's <ulink url=\"&url-"
"debian-projects;\">subprojects</ulink> which include ports to particular "
"architectures and <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> for specific user groups, among many others."
msgstr ""
"如果您能投入更多的时间的话，您可以负责维护 Debian 发行版中的部分自由软件。如"
"果您能够新增或维护其他用户希望 Debian 所能包含的软件包的话就更好了，可以在"
"<ulink url=\"&url-wnpp;\">急需人手和被期待的软件包库 （WNPP）</ulink>中获得相"
"关的信息。如果您对特定的用户群体感兴趣，那么您可能会很乐意参加 Debian 的某些"
"<ulink url=\"&url-debian-projects;\">子项目</ulink>，包括向特定处理器架构的移"
"植，以及为特定用户群体准备的 <ulink url=\"&url-debian-blends;\">Debian Pure "
"Blends</ulink>，等等。"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:128
msgid ""
"In any case, if you are working in the free software community in any way, "
"as a user, programmer, writer, or translator you are already helping the "
"free software effort.  Contributing is rewarding and fun, and as well as "
"allowing you to meet new people it gives you that warm fuzzy feeling inside."
msgstr ""
"无论如何，只要您以任何方式参加了自由软件社区的活动，不管您是用户、程序员、作"
"者，还是译者，您就已经为自由软件社区做出了贡献。贡献本身就是一件非常有益而有"
"趣的事情，它不仅让您能够结交更多的新伙伴，还能让您内心充满温暖的感觉。"
